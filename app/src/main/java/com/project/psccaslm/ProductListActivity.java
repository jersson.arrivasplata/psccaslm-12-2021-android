package com.project.psccaslm;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.psccaslm.adapters.OnItemClickListener;

public class ProductListActivity extends AppCompatActivity implements OnItemClickListener {
    private RecyclerView listRecyclerView;
    private static final String LOGTAG = "ProductListActivity";
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        context = this;

      //  listRecyclerView=(RecyclerView)findViewById(R.id.);
       // listRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

    }
    private void addContact(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_contact_label_text);
        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(R.string.add_contact_confirm_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(LOGTAG,"User clicked on OK");
            }
        });
        builder.setNegativeButton(R.string.add_contact_cancel_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(LOGTAG,"User clicked on Cancel");
                dialog.cancel();
            }
        });

        builder.show();

    }
    @Override
    public void OnItemClick(String contactJid) {
        Toast.makeText(context, "¡En construcción!", Toast.LENGTH_LONG).show();

    }
}
