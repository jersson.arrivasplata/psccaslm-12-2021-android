package com.project.psccaslm;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

public class ViewActivity extends AppCompatActivity{
    RecyclerView messagesRecyclerView;
    private EditText textSendEditText;
    private Button sendMessageButton;
    private String counterpartJid;
    private BroadcastReceiver mReceiveMessageBroadcastReceiver;
    private static final String LOGTAG = "ViewActivity" ;
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mQueue = Volley.newRequestQueue(this);

        Intent intent = getIntent();
        counterpartJid = intent.getStringExtra("contact_jid");
        setTitle(counterpartJid);

        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        Log.d("INICIA","ONcREATE");

    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d("INICIA","onStart");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.activity_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        return super.onOptionsItemSelected(item);
    }

    public void onClickBack(View v){
        super.onBackPressed();
    }

    private class ErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError error){
            Log.d("Response.Listener",error.toString());
        }
    }
}
