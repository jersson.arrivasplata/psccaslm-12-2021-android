package com.project.psccaslm.ui.keyboard;

public interface KeyboardVisibilityListener {
    void onKeyboardVisibilityChanged(boolean keyboardVisible);
}
