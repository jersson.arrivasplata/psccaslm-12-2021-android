package com.project.psccaslm.ui.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.psccaslm.ListActivity;
import com.project.psccaslm.R;
import com.project.psccaslm.network.routes;
import com.project.psccaslm.utils.CustomEditText;
import com.project.psccaslm.utils.DrawableClickListener;
import com.project.psccaslm.utils.Testing;
import com.project.psccaslm.utils.Utilies;

import java.util.HashMap;
import java.util.Map;


public class RegisterActivity extends AppCompatActivity{
    private String LOGTAG="";

    public static int statusLogin = 0;
    private LoginViewModel loginViewModel;
    private static final int REQUEST_READ_CONTACTS=0;
    EditText nameEditText=null;
    EditText emailEditText=null;
    CustomEditText passwordEditText=null;
    private BroadcastReceiver mBroadcastReceiver;
    public static Context context=null;
    private RequestQueue mQueue;
    private RequestQueue mQueueContact;

    private final int DURACION_SPLASH = 2000;
    int WAIT_TIME = 500;
    static Animation animTranslateX=null;
    static Animation animAlpha=null;
    public static String email="";
    public static String id="";
    public static String token="";
    TextView goToLogin=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register);
            findViewById(R.id.LoadId).setVisibility(View.GONE);
            findViewById(R.id.ContentId).setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable(){
                public void run(){
                    findViewById(R.id.LoadId).setVisibility(View.VISIBLE);
                    findViewById(R.id.ContentId).setVisibility(View.GONE);
                };
            }, DURACION_SPLASH);

            setTheme(R.style.AppTheme_Cursor);
            mQueue = Volley.newRequestQueue(this);
            mQueueContact = Volley.newRequestQueue(this);

            context = getApplicationContext();
            animTranslateX= AnimationUtils.loadAnimation(this,R.anim.translate_x);
            //
            loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                    .get(LoginViewModel.class);
            nameEditText= findViewById(R.id.name);
            emailEditText = findViewById(R.id.email);
            passwordEditText = findViewById(R.id.password);
            final Button loginButton = findViewById(R.id.login);
            final ProgressBar loadingProgressBar = findViewById(R.id.loading);

            passwordEditText.setDrawableClickListener(new DrawableClickListener() {
                @Override
                public void onClick(DrawablePosition target) {

                    if(passwordEditText.getTransformationMethod()==null){
                        passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
                    }else{
                        passwordEditText.setTransformationMethod(null);
                    }
                }
            });


    }
    public void goLogin(View view){
        Intent intent = new Intent(view.getContext(), LoginActivity.class);
        startActivity(intent);
    }
    public void login(View view){

        String name = nameEditText.getText().toString();

        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if(Testing.NO_VALIDATE_LOGIN==true){
            authentication(name,email,password);
        }else{

            if(name.isEmpty()){
                Toast.makeText(view.getContext(), "¡Complete el campo de Nombre!", Toast.LENGTH_LONG).show();
            }else
            if(email.isEmpty()){
                Toast.makeText(view.getContext(), "¡Complete el campo de Correo!", Toast.LENGTH_LONG).show();
            }else
            if(password.isEmpty()){
                Toast.makeText(view.getContext(), "¡Complete el campo de Password!", Toast.LENGTH_LONG).show();
            }else
            if(Utilies.emailValidator(email)==false){
                Toast.makeText(view.getContext(), "¡Recuerde ingresar un correo electrónico!", Toast.LENGTH_LONG).show();

            }
            else
            if(password.length()<6){
                Toast.makeText(view.getContext(), "¡Recuerde ingresar una contraseña de 6 caracteres mínimo!", Toast.LENGTH_LONG).show();

            }

            else
            if(email.isEmpty()==false && password.isEmpty()==false){
                authentication(name,email,password);
                //saveCredentialsAndLogin();
            }
             /*else
            if(Utilies.passwordValidator(password)==false){
                Toast.makeText(view.getContext(), "¡La contraseña debe contar con mayuscula, minuscula y números!", Toast.LENGTH_LONG).show();
            }*/
        }



    }

    public void authentication(final String name, final String email, final String password){
        StringRequest request = new StringRequest(
                Request.Method.POST,
                routes.Authregister,//getResources().getString(R.string.base_url)
                new ResponseListener(),
                new ErrorListener()) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("password", password);
                    params.put("roles", "USER");
                return params;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(
        2000,
        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mQueue.add(request);

    }

    public static String PREFS_KEY = "mispreferencias";
    private class ResponseListenerContact implements Response.Listener{
        @Override
        public void onResponse(Object response) {
            Log.d("JSON", String.valueOf(response));
            Gson gson = new Gson();
            JsonObject root = gson.fromJson(String.valueOf(response), JsonObject.class);

            JsonArray jsonArray = root.getAsJsonArray("data");
             RegisterActivity.email = root.get("email").getAsString();
            RegisterActivity.id = root.get("id").getAsString();


            Intent intent = new Intent(context, ListActivity.class);
            startActivity(intent);
        }
    }
    private class ResponseListener implements Response.Listener{
        @Override
        public void onResponse(Object response) {
            Log.d("JSON", String.valueOf(response));
            Gson gson = new Gson();
            JsonObject root = gson.fromJson(String.valueOf(response), JsonObject.class);

            SharedPreferences settings = getSharedPreferences(PREFS_KEY,MODE_PRIVATE);
            SharedPreferences.Editor editor;
            editor = settings.edit();

            boolean auth = Boolean.valueOf(root.get("auth").getAsBoolean());
            String token = String.valueOf(root.get("token"));

            editor.putBoolean("auth", auth);
            editor.putString("token", token);
            editor.commit();
            saveCredentialsAndLogin();
            //Toast.makeText(LoginActivity.this,String.valueOf(root.get("token")), Toast.LENGTH_LONG).show();
            Log.d("Response.Listener",String.valueOf(root.get("token")));

        }
    }
    private class ErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError error){
            Log.d("Response.Listener",error.toString());
        }
    }

    private void saveCredentialsAndLogin(){
        Log.d(LOGTAG,"saveCredentialsAndLogin() called.");
        SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(this);
        String email=emailEditText.getText().toString();
        if(email!=null){
            Intent intent = new Intent(context, ListActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void waitActivity(final Class cls){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                // yourMethod();
                Intent intent = new Intent(getApplicationContext(), cls);
                startActivity(intent);
            }
        }, WAIT_TIME);   //5 seconds
    }
    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = model.getDisplayName();//getString(R.string.welcome) +
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

}
