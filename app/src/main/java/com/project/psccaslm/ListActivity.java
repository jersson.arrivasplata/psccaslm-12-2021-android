package com.project.psccaslm;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bikomobile.multipart.MultipartRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.project.psccaslm.adapters.OnItemClickListener;
import com.project.psccaslm.adapters.ProductAdapter;
import com.project.psccaslm.dialogs.CustomDialog;
import com.project.psccaslm.model.Product;
import com.project.psccaslm.network.routes;
import com.project.psccaslm.retrofit.ModelClass.ImageSenderInfo;
import com.project.psccaslm.retrofit.NetworkRelatedClass.NetworkCall;
import com.project.psccaslm.ui.login.LoginActivity;
import com.project.psccaslm.utils.CustomEditText;
import com.project.psccaslm.utils.Testing;

import org.json.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ListActivity extends AppCompatActivity implements OnItemClickListener {
    RecyclerView recyclerView=null;
    private FloatingActionButton newButton;
    private static final String LOGTAG = "ListActivity";
    public Context context=null;
    boolean logged_in_state=false;
    private ListView listview;
    public static ArrayList<String> names;
    public static ArrayList<String> images;
    public static ArrayList<Product> products;
    public CustomEditText search;
    private static final int pic_id = 123;
    private RequestQueue mQueue;


    @Override
    protected void onStart() {
        super.onStart();
        context=this.getApplicationContext();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);
        context= this;
        mQueue = Volley.newRequestQueue(this);

        /*if(!logged_in_state)
        {
            //  this.logout();
           Log.d(LOGTAG,"Logged in state :"+ logged_in_state );
           Intent i = new Intent(ListActivity.this, LoginActivity.class);//redirigir
           startActivity(i);
           // finish();
        }*/
        listview = (ListView) findViewById(R.id.list_view);
        /*names = new ArrayList<String>();
        names.add("Veracruz");
        names.add("Tabasco");
        names.add("Chiapas");
        names.add("Campeche");*/
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_item_1, names);
        //listview.setAdapter(adapter);

        ProductAdapter adapter = new ProductAdapter(this, R.layout.item_product, products);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                CustomDialog dialog = new CustomDialog(products.get(position));
                dialog.show(getSupportFragmentManager(), "");

                //Window window = dialog.getDialog().getWindow();
              //  window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


               // Toast.makeText(ListActivity.this, "Has pulsado: "+ products.get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });
       // ProductAdapter myAdapter = new ProductAdapter(this, R.id.list_layout, names);
       // listview.setAdapter(myAdapter);

     //   recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
      //  recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        newButton=(FloatingActionButton)findViewById(R.id.new_conversation_floating_button);
        newButton.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        newButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(camera_intent, pic_id);

                   /* Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                    Intent i=new Intent(ListActivity.this, ProductListActivity.class);
                    startActivity(i);*/
                }
            });

        search = (CustomEditText)findViewById(R.id.search);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code

                //myadapter.getFilter().filter(s);
                //listview.setAdapter(myadapter);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayList<Product> myList = new ArrayList<>();
                int counter = 0;
                for(Product object: ListActivity.products){
                    if (object.getName().toLowerCase().contains(search.getText().toString().toLowerCase())){
                        myList.add(object);
                        counter = counter + 1;
                    }
                }
                if(counter==0){
                    myList = ListActivity.products;
                }
                ProductAdapter adapter = new ProductAdapter(context, R.layout.item_product, myList);
                listview.setAdapter(adapter);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

        });
    }
    // This method will help to retrieve the image
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {

        // Match the request 'pic id with requestCode
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == pic_id) {

            // BitMap is data structure of image file
            // which stor the image in memory
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            //thumbnail.compress(Bitmap.CompressFormat.JPEG,90,bytes);

            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File file = new File(directory, System.currentTimeMillis()+ ".jpg");
            if (!file.exists()) {
                Log.d("path", file.toString());
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.flush();
                    fos.close();
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }

//encode image to base64 string

            NetworkCall.fileUpload(file, new ImageSenderInfo("",18));
            this.consultImage(thumbnail);
           /* File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try{
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }*/


            //
            Toast.makeText(getApplicationContext(),"Image Saved",Toast.LENGTH_LONG).show();
            // Set the image in imageview for display
            //  click_image_id.setImageBitmap(photo);
        }
    }
   /* public void abrirCamara (){
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i,0);
        ocultar();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMARA : {
                // Si la petición es cancelada, el array resultante estará vacío.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // El permiso ha sido concedido.
                    abrirCamara ();
                } else {
                    // Permiso denegado, deshabilita la funcionalidad que depende de este permiso.
                }
                return;
            }
            // otros bloques de 'case' para controlar otros permisos de la aplicación
        }
    }*/

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
      //  savedInstanceState.putString(STATE_USER, mUser);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_me_menu01, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnItemClick(String contactJid) {
        Intent i=new Intent(ListActivity.this, ViewActivity.class);
        i.putExtra("contact_jid",contactJid);
        startActivity(i);
    }
    public void logoutAll(View view){
       this.logout();
    }
    public  void logout(){

        Intent intent = new Intent(ListActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void consultImage(final Bitmap file){

        HashMap<String, String> params = new HashMap<String, String>();
        String REGISTER_URL = routes.AuthConsultImage;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        file.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();

        final String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(ListActivity.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ListActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("file", encoded);
                //params.put(KEY_PASSWORD,password);
                //params.put(KEY_EMAIL, email);
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mQueue.add(stringRequest);
    }

    private class ErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError error){
            Log.d("Response.Listener",error.toString());
        }
    }


    private class ResponseListenerProduct implements Response.Listener<String>{
        @Override
        public void onResponse(String response) {
            Log.d("JSON", String.valueOf(response));

        }
    }

}
