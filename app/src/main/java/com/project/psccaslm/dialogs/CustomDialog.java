package com.project.psccaslm.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.project.psccaslm.R;
import com.project.psccaslm.adapters.ProductAdapter;
import com.project.psccaslm.model.Product;
import com.project.psccaslm.network.routes;
import com.project.psccaslm.utils.Utilies;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

public class CustomDialog extends DialogFragment {
    Product product;
    public CustomDialog(Product product) {
        this.product = product;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View view = inflater.inflate(R.layout.dialog_product, null);
        builder.setView(view);

        String name = this.product.getName();
        String image = this.product.getImage();
        String description = this.product.getAbst();

        TextView descriptionTextView = (TextView) view.findViewById(R.id.description);
        descriptionTextView.setText(description);



        String nutritional = this.product.getNutritional();
        JsonParser parser = new JsonParser();
        JsonElement tradeElement = parser.parse(nutritional);
        JsonArray myJSONArray = tradeElement.getAsJsonArray();
        TextView nameTextView = (TextView) view.findViewById(R.id.name);
        nameTextView.setText(name);

        LinearLayout linearLayout = view.findViewById(R.id.dialogproduct_linear);

        linearLayout.setOrientation(LinearLayout.VERTICAL);
        for (JsonElement pa : myJSONArray) {
            TextView textView = new TextView(builder.getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            textView.setLayoutParams(lp);
            textView.setTextSize(16);
            textView.setPadding(20,10,20,10);
            //or to support all versions use
            Typeface typeface = ResourcesCompat.getFont(builder.getContext(), R.font.sfprodisplaymedium);
            textView.setTypeface(typeface);
            textView.setTextColor(getResources().getColor(R.color.colorMainBlack));

            ProgressBar progressBar = new ProgressBar(builder.getContext(), null, 0, R.style.CustomProgressBarHorizontal);
            progressBar.setPadding(20,10,20,10);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(20,10,20,10);
            progressBar.setLayoutParams(params);
            progressBar.setScaleY(2f);

            String nameNutritional = null;
            String valueNutritional = null;
            JsonObject paymentObj = pa.getAsJsonObject();
             nameNutritional     = paymentObj.get("name").getAsString();
             valueNutritional = paymentObj.get("value").getAsString();

            textView.setText(nameNutritional + " - "+ valueNutritional+ "%");



            linearLayout.addView(textView);
            progressBar.setProgress(Integer.parseInt(valueNutritional));
            linearLayout.addView(progressBar);


        }

        new Utilies.DownloadImageTask((ImageView) view.findViewById(R.id.image))
                .execute(routes.urlServer+image);



     //   AlertDialog alertDialog =  builder.create();


        return builder.create();
    }


}
