package com.project.psccaslm.model;

public enum SubscriptionType {
    //Subscription type should catter for the from and to channels. We should simultaneously know the FROM and TO subscription information
    //FROM  - TO
    NONE_NONE,//No presence subscription
    NONE_PENDING,
    NONE_TO,

    PENDING_NONE,
    PENDING_PENDING,
    PENDING_TO,

    FROM_NONE,
    FROM_PENDING,
    FROM_TO
}
