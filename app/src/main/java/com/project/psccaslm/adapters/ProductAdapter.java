package com.project.psccaslm.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.psccaslm.R;
import com.project.psccaslm.model.Product;
import com.project.psccaslm.network.routes;
import com.project.psccaslm.utils.Utilies;

import java.io.InputStream;
import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<String> names;
    private ArrayList<String> images;
    private ArrayList<Product> products;

    public ProductAdapter(Context context, int layout, ArrayList<Product> products){
        this.context = context;
        this.layout = layout;
        this.products = products;
    }

    @Override
    public int getCount() {
        return this.products.size();
    }

    @Override
    public Object getItem(int position) {
        return this.products.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup viewGroup) {
        // Copiamos la vista
        View v = convertView;

        //Inflamos la vista con nuestro propio layout
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);

        v= layoutInflater.inflate(R.layout.item_product, null);
        // Valor actual según la posición

        String name  = products.get(position).getName();
        String image  = products.get(position).getImage();


        // Referenciamos el elemento a modificar y lo rellenamos
        TextView textView = (TextView) v.findViewById(R.id.name);
        textView.setText(name);
        new Utilies.DownloadImageTask((ImageView) v.findViewById(R.id.image))
                .execute(routes.urlServer+image);
        //Devolvemos la vista inflada
        return v;
    }


}
