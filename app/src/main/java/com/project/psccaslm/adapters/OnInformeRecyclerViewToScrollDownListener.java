package com.project.psccaslm.adapters;

public interface OnInformeRecyclerViewToScrollDownListener {
    public void onInformeRecyclerViewToScrollDown(int size);
}
